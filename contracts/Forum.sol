contract Forum {

	struct Post {
		string title;
		string content;
		address user;
		uint commentCount;
		uint[] comments;
	}
	
	struct comment {
		string content;
		address user;
        uint reputation;
        uint parentPostId;
	}

	struct user {
	    string name;
	    uint reputation;
	    bool exists;
	}

     event UpdateReputation(uint commentReputation, uint authorReputation, string commentAuthor);
     event GiveComment(string userName, uint commentId, string content, uint reputation);
	 event AnnounceAddComment(string userName, uint commentId, string content, uint reputation);
	 event AnnounceAddPost(string userName, string title, string content, uint postId, uint commentCount);
	 event GivePost(string userName, string title, string content, uint postId, uint commentCount, uint[] commentIds);

    mapping(uint => Post) posts;
    mapping(uint => comment) comments;
	mapping (address => user) addressToUserMapping;
	mapping (string => address) stringToAddressMapping;

    uint postCount = 0;
    uint commentCount = 0;

    function getNumComments(uint parentPostID) returns (uint numberComments) {
            Post parentPost = posts[parentPostID];

            numberComments = parentPost.comments.length;
            return numberComments;
    }

     function getPosts()  {
            for (uint i=0; i< postCount; i++) {
               GivePost(addressToUserMapping[posts[i].user].name, posts[i].title, posts[i].content, i,
               posts[i].commentCount, posts[i].comments);
            }

     }

     function getComments(uint parentPostID) {
           Post parentPost = posts[parentPostID];

           for (uint i=0; i<parentPost.comments.length; i++) {
                uint id = parentPost.comments[i];

                GiveComment(addressToUserMapping[comments[id].user].name, id, comments[id].content, comments[id].reputation);
           }
     }


	function containsUser(string userName) returns (bool containsUser) {

    		if  (stringToAddressMapping[userName] == 0) {
    		    return false;
    		} else {
    		    return true;
    		}
    }

	function getReputation(address userAddress) returns (uint reputation) {
	   return addressToUserMapping[userAddress].reputation;
	}

	function exists(address userAddress) returns (bool exists) {
	    if (addressToUserMapping[userAddress].exists == true) {
	        return true;
	    } else {
	        return false;
	    }
	}

	function addUser(string userName, address userAddress) {
        stringToAddressMapping[userName] = userAddress;
        addressToUserMapping[userAddress].exists = true;
        addressToUserMapping[userAddress].reputation = 0;
        addressToUserMapping[userAddress].name = userName;

	}


	function getNextAvailableAddress(address[] addresses, string userName) returns (address nextAddress) {
        for (uint i=1; i<addresses.length; i++) {
            nextAddress = addresses[i];
            if (addressToUserMapping[nextAddress].exists == false) {
                return nextAddress;
            }
	    }
	}

	function getAddress(string userName) returns (address userAddress) {
	    return stringToAddressMapping[userName];
	}

    function getPostCount() returns (uint count) {
        return postCount;
    }

    function debug() returns (uint num) {
       postCount++;
        num = postCount;
    }

    function setPostCount(uint newPostCount) {
        postCount = newPostCount;
    }


    function addNewPost(address userAddress, string title, string content) {

            uint postId = postCount++;
    		posts[postId].title = title;
    		posts[postId].content = content;
    		posts[postId].user = userAddress;
            posts[postId].commentCount = 0;

            AnnounceAddPost(addressToUserMapping[userAddress].name, title, content, postId, 0);
    }

    function addNewComment(address userAddress, string content, uint parentPostId) {

            uint commentId = commentCount++;
    		comments[commentId].user = userAddress;
    		comments[commentId].content = content;
    		comments[commentId].reputation = 0;
            comments[commentId].parentPostId = parentPostId;

    		posts[parentPostId].comments.push(commentId);
    		posts[parentPostId].commentCount++;

            AnnounceAddComment(addressToUserMapping[userAddress].name, commentId, content, 0);
    }

	 function incrementReputation(uint commentId, string author) {
	        comments[commentId].reputation++;
            uint commentRep = comments[commentId].reputation;
            addressToUserMapping[comments[commentId].user].reputation++;
            uint authorRep = addressToUserMapping[comments[commentId].user].reputation;

            UpdateReputation(commentRep, authorRep, author);
     }

	 function decrementReputation(uint commentId, string author) {
	        comments[commentId].reputation--;
            uint commentRep = comments[commentId].reputation;
            addressToUserMapping[comments[commentId].user].reputation--;
            uint authorRep = addressToUserMapping[comments[commentId].user].reputation;

            UpdateReputation(commentRep, authorRep, author);
	 }
}