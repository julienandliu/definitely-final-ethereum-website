module.exports = {
    build: {
		"app.js": [
      "javascripts/app.js"
    ],
        "index.html": "index.html",
        "home.html": "home.html",
        "post.html": "post.html",
        "js/jquery-1.12.3.js": "js/jquery-1.12.3.js",
        "js/account.js": "js/account.js",
        "js/home.js": "js/home.js",
        "js/post.js": "js/post.js",
        "css/style.css": "css/style.css",
        "css/index.css": "css/index.css",
        "css/home.css": "css/home.css",
        "css/post.css": "css/post.css"
    },
    deploy: [
        "Forum"
    ],
    rpc: {
        host: "localhost",
        port: 8545
    }
};
