# Ethereum-Overflow

Ethereum Overflow is a forum based on Stack OverFlow and run on the Ethereum Blockchain. To run the source code you will need the following installed

  - testrpc
  - truffle

To start running the application you need to do the following things:
  - run testrpc (this is to simulate an ethereum node to which the contract can be deployed on, and functions can be run)
  - in the root folder, 'truffle deploy'
  - Open index.html in environments/development/build/index.html

Team:
Romain Julien, Millie MacDonald, Lisa Liu-Thorrold