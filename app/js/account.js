var forum;
var userName = "";
var userAddress = null;
var userReputation = null;

$(document).ready(function() {

    web3.eth.defaultAccount = web3.eth.accounts[0];

    var init = window.localStorage.getItem("init");
    if (init !== undefined && init != null && init == "back") {
        window.localStorage.setItem("init", "whatever");
        forum = Forum.at(window.localStorage.getItem("contract"));
        return;
    } else if (init == "whatever") {
        setContractAccess();
    }
});

function setContractAccess() {
    // Connect to the contract
    forum = Forum.at(Forum.deployed_address);
    var myForum = forum;

    web3.eth.defaultAccount = web3.eth.accounts[0];

    Forum.new().then(
        function(conf) {
            window.localStorage.setItem("contract", conf.address);
            forum = Forum.at(conf.address);
            myForum = conf;
            // watch for changes
            console.log('Forum created');
        }, function(err) {
            console.log(err);
        });
}

function createAccount(event, input) {
    if(event.keyCode != 13) {
        console.log("not enter");
        return;
    }
    console.log("ENTER");

    var inputField = $( input );
    userName = inputField.val();

    userAddress = forum.getNextAvailableAddress.call(web3.eth.accounts, userName.trim());
    userReputation = userAddress.then(function(address) {
        forum.addUser(userName.trim(), address);
        return forum.getReputation.call(address);
    });

}

function saveAccountInfo() {
    if (userName == "" || userAddress == null || userReputation == null) {
        console.log("NOT GOING ANYWHERE");
        console.log("username: " + userName);
        console.log("userAddress: " + userAddress.value());
        console.log("userReputation : " + userReputation.value());
        return false;
    }

    var storage = window.localStorage;
    storage.setItem("userName", userName);
    storage.setItem("userAddress", userAddress.value());
    storage.setItem("userReputation", userReputation.value());

    console.log("username: " + userName);
    console.log("userAddress: " + userAddress.value());
    console.log("userReputation : " + userReputation.value());


    return true;
}

function login(event, input) {
    if(event.keyCode != 13) {
        console.log("not enter");
        return;
    }
    console.log("ENTER");

    var inputField = $( input );
    userName = inputField.val();

    userAddress = forum.getAddress.call(userName.trim());
    userReputation = userAddress.then(function(address) {
        return forum.getReputation.call(address);
    });

}
