var forum;
var posts = [];

$(document).ready(function() {
    web3.eth.defaultAccount = web3.eth.accounts[0];
    forum = Forum.at(window.localStorage.getItem("contract"));

    forum.AnnounceAddPost().watch(function(error, event) {
        if (!error) {
            announcePost(event.args);
        } else {
            console.log('error occurred', err);
        }
    });


    forum.GivePost().watch(function(error, event) {

        if (!error) {
            getPostsFromBlockChain(event.args);
            console.log("Forum Give Post fired");
        } else {
            console.log('error occurred', err);
        }
    });

    setLoginInfo();
    forum.getPosts();
});

function getPostsFromBlockChain(data) {
    posts[data.postId] = {};
    posts[data.postId].title = data.title;
    posts[data.postId].user = data.userName;
    posts[data.postId].content = data.content;
    posts[data.postId].commentCount = data.commentCount.c[0];

    var html = getQuestionhtml(posts[data.postId].commentCount,
        posts[data.postId].title, posts[data.postId].content, posts[data.postId].user, data.postId);
    $( "#main" ).append(html);

    console.log(posts[data.postId]);
}

function announcePost(data) {
    posts[data.postId] = {};
    posts[data.postId].title = data.title;
    posts[data.postId].user = data.userName;
    posts[data.postId].content = data.content;
    posts[data.postId].commentCount = data.commentCount.c[0];

    var html = getQuestionhtml(posts[data.postId].commentCount,
        posts[data.postId].title, posts[data.postId].content, posts[data.postId].user, data.postId);
    $( "#main" ).append(html);

    console.log(posts[data.postId]);
}


function setLoginInfo() {
    var storage = window.localStorage;
    var userName = storage.getItem("userName");
    var userAddress = storage.getItem("userAddress");
    var userReputation = storage.getItem("userReputation");
    
    var loginInfoDiv = $( "#login-info" );
    loginInfoDiv.children( ".username" ).text("User: " + userName);
    loginInfoDiv.children( ".userAddress" ).text("Address: " + userAddress);
    loginInfoDiv.children( ".userReputation" ).text("Reputation: " + userReputation);
}

function logout() {
    var storage = window.localStorage;
    storage.setItem("userName", null);
    storage.setItem("init", "back");
    window.location.href = "index.html";
}

function getQuestionhtml(nbrComments, title, content, author, postId) {
    var html = "";
    html += "<div class='question'>";
    html += "<div class='comments'>";
    html += "<div class='square-wrapper'>";
    html += "<p class='number-comments'>";
    html += nbrComments;
    html += "</p>";
    html += "<p>Comments</p>";
    html += "</div>";
    html += "</div>";

    html += "<a href='post.html'><div class='post' onclick='saveSelectedPost(this)'>";
    html += "<h2>";
    html += title;
    html += "</h2>";
    html += "<p class='content'>";
    html += content;
    html += "</p>";
    html += "<p class='author'>";
    html += author;
    html += "</p>";
    html += "<div class='post-id'>";
    html += postId;
    html += "</div>";
    html += "</div></a>";
    html += "</div>";

    html += "<div style='clear: both'></div>";

    return html;
}

function saveSelectedPost(post) {
    var storage = window.localStorage;
    var postDiv = $( post );

    var title = postDiv.children( "h2" ).html();
    storage.setItem("postTitle", title);

    var content = postDiv.children( ".content" ).html();
    storage.setItem("postContent", content);

    var author = postDiv.children( ".author" ).html();
    storage.setItem("postAuthor", author);

    var postId = postDiv.children( ".post-id" ).html();
    storage.setItem("postId", postId);
}


// ---------------------------------------------------------------------------------------------
// -------------------------------------- CREATE NEW POST --------------------------------------
// ---------------------------------------------------------------------------------------------

function addNewPost() {
    var inputFieldTitle = $( "#new-post" ).children( "input" );
    var textareaFieldContent = $( "#new-post" ).children( "textarea" );

    var author = window.localStorage.getItem("userName");
    var title = inputFieldTitle.val();
    var content = textareaFieldContent.val();
    content = content.replace(new RegExp('\n', 'g'), '<br/>');

    var postCount = forum.getPostCount.call();

    var userAddress = postCount.then(function (count) {
        console.log("Count: " + count.c[0]);
        return forum.getAddress.call(author);
    });

    var id = userAddress.then(function(address) {
        console.log("got address back: " + address);
        forum.addNewPost(address, title, content, 0);
        return null;
    });
    

    hideNewProjectModal();
}

function showNewProjectModal() {
    var top, left;
    $("#new-post-block-view").css("display", "block");
    var $modal = $("#new-post");

    top = Math.max($(window).height() - $modal.outerHeight(), 0) / 2;
    left = Math.max($(window).width() - $modal.outerWidth(), 0) / 2;

    $modal.css({top:top, left:left});
}

function hideNewProjectModal() {
    $("#new-post-block-view").css("display", "none");
}