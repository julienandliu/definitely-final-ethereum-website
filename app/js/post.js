var postId;
var postAuthor;
var postAuthorAddress;
var postTitle;
var postContent;
var postAuthorReputation;
var comments = [];

var updatedButton;

var forum;

$(document).ready(function() {
    var storage = window.localStorage;
    web3.eth.defaultAccount = web3.eth.accounts[0];
    forum = Forum.at(storage.getItem("contract"));

    forum.AnnounceAddComment().watch(function(error, event) {
        if (!error) {
            announceComment(event.args);
        } else {
            console.log('error occurred', err);
        }
    });

    forum.GiveComment().watch(function(error, event) {

        if (!error) {
            getCommentsFromBlockChain(event.args);
            console.log("Forum Give Post fired");
        } else {
            console.log('error occurred', err);
        }
    });

    forum.UpdateReputation().watch(function(error, event) {
        if (!error) {
            updateReputation(event.args);
        } else {
            console.log('error occurred', err);
        }
    });

    postId = storage.getItem("postId");
    postTitle = storage.getItem("postTitle");
    postContent = storage.getItem("postContent");
    postAuthor = storage.getItem("postAuthor");

    var addressObj = forum.getAddress.call(postAuthor);
    var reputationObj = addressObj.then(function(address) {
        postAuthorAddress = address;
        return forum.getReputation.call(address);
    });
    var dummy = reputationObj.then(function(reputation) {
        postAuthorReputation = reputation;
        setPostQuestion()
    });

    setLoginInfo();

    forum.getComments(postId);

    console.log("Post id" + postId);

    console.log("Comment count1 " + forum.getNumComments(postId));
    console.log("Comment count2" + forum.getNumComments.call(postId));
});

function getCommentsFromBlockChain(data) {

    //     event GiveComment(string userName, uint commentId, string content, uint reputation);

    comments[data.commentId] = {};
    comments[data.commentId].userName = data.userName;
    comments[data.commentId].commentId = data.commentId;
    comments[data.commentId].content = data.content;
    comments[data.commentId].reputation = data.reputation.c[0];

    var addressObj = forum.getAddress.call(comments[data.commentId].userName);
    var reputationObj = addressObj.then(function(address) {
        return forum.getReputation.call(address);
    });
    var dummy = reputationObj.then(function(reputation) {
        var commentAuthorReputation = reputation;
        // Add the new post at the end of the list
        var html = getCommentHtml(true, comments[data.commentId].reputation, "", comments[data.commentId].content,
            comments[data.commentId].userName, commentAuthorReputation, data.commentId);
        $( "#main" ).append(html);
    });

    console.log(comments[data.commentId]);
}

function announceComment(data) {
    var commentId = data.commentId.c[0];
    var content = data.content;
    var commentRep = data.reputation.c[0];
    var author = window.localStorage.getItem("userName");

    var repDivContent = $( "#login-info" ).children( ".userReputation" ).html();
    var tokens = repDivContent.split(" ");
    var userReputation = tokens[1];

    // Add the new post at the end of the list
    var html = getCommentHtml(true, commentRep, "", content, author, userReputation, commentId);
    $( "#main" ).append(html);
}

function updateReputation(data) {
    var commentRep = data.commentReputation;
    var authorRep = data.authorReputation;
    var commentAuthor = data.commentAuthor;

    var rateDiv = updatedButton.siblings( ".rate" );
    var rate = Number(updatedButton.siblings( ".rate" ).html());
    rateDiv.text(commentRep);

    $( ".author" ).each(function( index ) {
        var tokens = $( this ).html().split(" ");
        var author = tokens[0];
        if (author == commentAuthor) {
            $( this ).text(author + " ( " + authorRep + " )");
        }
    });

    if (window.localStorage.getItem("userName") == commentAuthor) {
        $( "#login-info" ).children( ".userReputation" ).text("Reputation: " + authorRep);
        window.localStorage.setItem("userReputation", authorRep);
    }
}

function logout() {
    var storage = window.localStorage;
    storage.setItem("userName", null);
    storage.setItem("init", "back");
    window.location.href = "index.html";
}


function setLoginInfo() {
    var storage = window.localStorage;
    var userName = storage.getItem("userName");
    var userAddress = storage.getItem("userAddress");
    var userReputation = storage.getItem("userReputation");

    var loginInfoDiv = $( "#login-info" );
    loginInfoDiv.children( ".username" ).text("User: " + userName);
    loginInfoDiv.children( ".userAddress" ).text("Address: " + userAddress);
    loginInfoDiv.children( ".userReputation" ).text("Reputation: " + userReputation);
}

function setPostQuestion() {
    var question = getCommentHtml(false, 0, postTitle, postContent, postAuthor, postAuthorReputation, postId);

    var mainDiv = $( "#main" );
    mainDiv.append(question);
}

function getCommentHtml(hasRating, rate, title, content, author, reputation, commentId) {
    var html = "";

    html += "<div class='post'>";
        html += "<div class='rating'>";
    if (hasRating) {
            html += "<div class='button' onclick='upVote(this)'>Up</div>";
            html += "<div class='rate'>";
                html += rate;
            html += "</div>";
            html += "<div class='button' onclick='downVote(this)'>Down</div>";
    }
        html += "</div>";
        html += "<div class='post-block'>";
    if (!hasRating) {
            html += "<h2>";
                html += title;
            html += "</h2>";
    }
            html += "<p>";
                html += content;
            html += "</p>";
            html += "<p class='author'>";
                html += author + " ( " + reputation + " )";
            html += "</p>";
        html += "</div>";
        html += "<div class='comment-id'>";
            html += commentId;
        html += "</div>";
    html += "</div>";

    html += "<div style='clear: both'></div>";

    return html;
}

function upVote(button) {
    updatedButton = $( button );

    var commentId = updatedButton.parent().siblings( ".comment-id" ).html();
    var authorDiv = updatedButton.parent().siblings( ".post-block" ).children( ".author" );
    var tokens = authorDiv.html().split(" ");
    var author = tokens[0];

    if (author != window.localStorage.getItem("userName")) {
        forum.incrementReputation(commentId, author);
    }
}

function downVote(button) {
    updatedButton = $( button );

    var commentId = updatedButton.parent().siblings( ".comment-id" ).html();
    var authorDiv = updatedButton.parent().siblings( ".post-block" ).children( ".author" );
    var tokens = authorDiv.html().split(" ");
    var author = tokens[0];
    var rate = updatedButton.siblings( ".rate" ).html();
    if (rate == "0") {
        return;
    }

    if (author != window.localStorage.getItem("userName")) {
        forum.decrementReputation(commentId, author);
    }
}

// ---------------------------------------------------------------------------------------------
// -------------------------------------- ADD NEW COMMENT --------------------------------------
// ---------------------------------------------------------------------------------------------

function addNewComment() {
    var textareaFieldContent = $( "#new-comment" ).children( "textarea" );
    var content = textareaFieldContent.val();
    content = content.replace(new RegExp('\n', 'g'), '<br/>');
    
    var userAddress = forum.getAddress.call(window.localStorage.getItem("userName"));

    var id = userAddress.then(function(address) {
        console.log("got address back: " + address);
        forum.addNewComment(address, content, postId);
        return null;
    });

    hideNewCommentModal();
}

function showNewCommenttModal() {
    var top, left;
    $("#new-comment-block-view").css("display", "block");
    var $modal = $("#new-comment");

    top = Math.max($(window).height() - $modal.outerHeight(), 0) / 2;
    left = Math.max($(window).width() - $modal.outerWidth(), 0) / 2;

    $modal.css({top:top, left:left});
}

function hideNewCommentModal() {
    $("#new-comment-block-view").css("display", "none");
}